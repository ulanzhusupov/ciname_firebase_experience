import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:cinema_firebase_edition/resources/resources.dart';

void main() {
  test('images assets test', () {
    expect(File(Images.cinemaPoster).existsSync(), isTrue);
    expect(File(Images.dune).existsSync(), isTrue);
    expect(File(Images.noImage).existsSync(), isTrue);
    expect(File(Images.squad).existsSync(), isTrue);
    expect(File(Images.theTomorrowWar).existsSync(), isTrue);
    expect(File(Images.wrathOfMan).existsSync(), isTrue);
  });
}
