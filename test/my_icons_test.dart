import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:cinema_firebase_edition/resources/resources.dart';

void main() {
  test('my_icons assets test', () {
    expect(File(MyIcons.accountCircle).existsSync(), isTrue);
    expect(File(MyIcons.bgExplore).existsSync(), isTrue);
    expect(File(MyIcons.bgTop).existsSync(), isTrue);
    expect(File(MyIcons.film).existsSync(), isTrue);
    expect(File(MyIcons.filterSolid).existsSync(), isTrue);
    expect(File(MyIcons.search).existsSync(), isTrue);
    expect(File(MyIcons.searchBlack).existsSync(), isTrue);
    expect(File(MyIcons.swipe).existsSync(), isTrue);
    expect(File(MyIcons.tickets).existsSync(), isTrue);
  });
}
