import 'package:auto_route/auto_route.dart';
import 'package:cinema_firebase_edition/presentation/pages/auth/login_page.dart';
import 'package:cinema_firebase_edition/presentation/pages/auth/registration_page.dart';
import 'package:cinema_firebase_edition/presentation/pages/home_page.dart';
import 'package:cinema_firebase_edition/presentation/pages/movie_page.dart';
import 'package:flutter/material.dart';
part 'app_router.gr.dart';

@AutoRouterConfig()
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
        /// routes go here
        AutoRoute(
          page: HomeRoute.page,
          initial: true,
        ),
        AutoRoute(
          page: LoginRoute.page,
        ),
        AutoRoute(
          page: RegistrationRoute.page,
        ),
        AutoRoute(
          page: MovieRoute.page,
        ),
      ];
}
