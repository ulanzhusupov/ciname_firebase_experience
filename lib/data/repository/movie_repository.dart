import 'package:cinema_firebase_edition/core/network/dio_settings.dart';
import 'package:cinema_firebase_edition/data/models/movie_details_model.dart';
import 'package:cinema_firebase_edition/data/models/movies_model.dart';
import 'package:dio/dio.dart';
import 'package:cinema_firebase_edition/core/constants/variables.dart';

class MovieRepository {
  DioSettings dioSettings;

  MovieRepository({required this.dioSettings});

  Future<MoviesModel> getToRatedTV() async {
    final result = await dioSettings.dio.get(
      "https://api.themoviedb.org/3/tv/top_rated?language=ru-RU&page=1",
      options: Options(
        headers: {"Authorization": "Bearer ${Variables.API_TOKEN}"},
      ),
    );

    return MoviesModel.fromJson(result.data);
  }

  Future<MoviesModel> getMoviesFor2024() async {
    final result = await dioSettings.dio.get(
      "https://api.themoviedb.org/3/discover/movie?include_adult=false&include_video=false&language=ru-RU&page=1&primary_release_year=2024&sort_by=popularity.desc",
      options: Options(
        headers: {"Authorization": "Bearer ${Variables.API_TOKEN}"},
      ),
    );

    return MoviesModel.fromJson(result.data);
  }

  Future<MoviesModel> getPopularMovies() async {
    final result = await dioSettings.dio.get(
      "https://api.themoviedb.org/3/movie/popular?language=ru-RU&page=1",
      options: Options(
        headers: {"Authorization": "Bearer ${Variables.API_TOKEN}"},
      ),
    );

    return MoviesModel.fromJson(result.data);
  }

  Future<MoviesModel> getMovieBySearch(String query) async {
    final result = await dioSettings.dio.get(
      "https://api.themoviedb.org/3/search/movie?query=$query&language=ru-RU",
      options:
          Options(headers: {"Authorization": "Bearer ${Variables.API_TOKEN}"}),
    );

    return MoviesModel.fromJson(result.data);
  }

  Future<MovieDetailsModel> getMovieDetailsById(int id) async {
    final result = await dioSettings.dio.get(
      "https://api.themoviedb.org/3/movie/$id?language=ru-RU",
      options:
          Options(headers: {"Authorization": "Bearer ${Variables.API_TOKEN}"}),
    );

    return MovieDetailsModel.fromJson(result.data);
  }

  Future<MovieDetailsModel> getTvDetailsById(int id) async {
    final result = await dioSettings.dio.get(
      "https://api.themoviedb.org/3/tv/$id?language=ru-RU",
      options:
          Options(headers: {"Authorization": "Bearer ${Variables.API_TOKEN}"}),
    );

    return MovieDetailsModel.fromJson(result.data);
  }
}
