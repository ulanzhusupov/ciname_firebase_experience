class TinderMovieCardModel {
  final String title;
  final String year;
  final String image;
  final String rating;

  TinderMovieCardModel(
      {required this.title,
      required this.year,
      required this.image,
      required this.rating});
}
