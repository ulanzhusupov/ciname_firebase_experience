import 'package:cinema_firebase_edition/data/models/movie_details_model.dart';
import 'package:cinema_firebase_edition/data/models/movies_model.dart';
import 'package:cinema_firebase_edition/data/repository/movie_repository.dart';
import 'package:cinema_firebase_edition/hive/hive_movie_model.dart';
import 'package:dio/dio.dart';
import 'package:hive/hive.dart';

class MoviesUsecase {
  final MovieRepository repository;

  MoviesUsecase({required this.repository});

  List<HiveMovieModel>? get movies => _movies;
  MovieDetailsModel? get movieDetails => _movieDetails;
  List? get favoriteMovies => _favoriteMovies;
  List<HiveMovieModel>? get moviesFor2024 => _moviesFor2024;
  List<HiveMovieModel>? get topRatedTV => _topRatedTV;

  List? _favoriteMovies;
  List<HiveMovieModel>? _movies;
  MovieDetailsModel? _movieDetails;
  List<HiveMovieModel>? _moviesFor2024;
  List<HiveMovieModel>? _topRatedTV;

  Future<void> getFavoriteMovies() async {
    try {
      var moviesBox = await Hive.openBox("moviesBox");
      _favoriteMovies = await moviesBox.get("likedMovies");
    } catch (e) {
      if (e is HiveError) {
        throw HiveError(e.message);
      }
    }
  }

  Future<void> getExploreMovies() async {
    try {
      MoviesModel popularMoviesModel = await repository.getPopularMovies();
      MoviesModel moviesFor2024Model = await repository.getMoviesFor2024();
      MoviesModel topRatedTvModel = await repository.getToRatedTV();

      _movies = popularMoviesModel.results;
      _moviesFor2024 = moviesFor2024Model.results;
      _topRatedTV = topRatedTvModel.results;
    } catch (e) {
      if (e is HiveError) {
        throw HiveError(e.message);
      } else if (e is DioException) {
        throw DioException(
            requestOptions: e.requestOptions, message: e.message);
      }
    }
  }

  Future<void> getPopularMovies() async {
    try {
      MoviesModel popularMoviesModel = await repository.getPopularMovies();
      _movies = popularMoviesModel.results;
    } catch (e) {
      if (e is HiveError) {
        throw HiveError(e.message);
      } else if (e is DioException) {
        throw DioException(
            requestOptions: e.requestOptions, message: e.message);
      }
    }
  }

  Future<void> getPopularMoviesInSwipe() async {
    try {
      MoviesModel popularMoviesModel = await repository.getPopularMovies();

      var moviesBox = await Hive.openBox("moviesBox");
      List likedFromHive = await moviesBox.get("likedMovies");

      _movies = popularMoviesModel.results
              ?.where(
                  (obj1) => !likedFromHive.any((obj2) => obj2.id == obj1.id))
              .toList() ??
          [];
    } catch (e) {
      if (e is HiveError) {
        throw HiveError(e.message);
      } else if (e is DioException) {
        throw DioException(
            requestOptions: e.requestOptions, message: e.message);
      }
    }
  }

  Future<void> getMoviesBySearch(String query) async {
    try {
      MoviesModel searchMoviesModel = await repository.getMovieBySearch(query);

      _movies = searchMoviesModel.results;
      _movies
          ?.sort((a, b) => (b.voteAverage ?? 0).compareTo(a.voteAverage ?? 0));
    } catch (e) {
      if (e is HiveError) {
        throw HiveError(e.message);
      } else if (e is DioException) {
        throw DioException(
            requestOptions: e.requestOptions, message: e.message);
      }
    }
  }

  Future<void> getMovieDetailsById(int id) async {
    try {
      _movieDetails = await repository.getMovieDetailsById(id);
    } catch (e) {
      if (e is HiveError) {
        throw HiveError(e.message);
      } else if (e is DioException) {
        throw DioException(
            requestOptions: e.requestOptions, message: e.message);
      }
    }
  }

  Future<void> getTvDetailsById(int id) async {
    try {
      _movieDetails = await repository.getTvDetailsById(id);
    } catch (e) {
      if (e is HiveError) {
        throw HiveError(e.message);
      } else if (e is DioException) {
        throw DioException(
            requestOptions: e.requestOptions, message: e.message);
      }
    }
  }
}
