import 'package:cinema_firebase_edition/data/models/movie_details_model.dart';
import 'package:cinema_firebase_edition/hive/hive_movie_model.dart';

sealed class MoviesState {}

final class MoviesInitial extends MoviesState {}

final class MoviesLoading extends MoviesState {}

final class MoviesSuccess extends MoviesState {
  final List<HiveMovieModel>? movies;
  final MovieDetailsModel? movieDetails;
  final List<HiveMovieModel>? moviesFor2024;
  final List<HiveMovieModel>? topRatedTV;
  final List? favoriteMovies;

  MoviesSuccess({
    this.movies,
    this.movieDetails,
    this.moviesFor2024,
    this.topRatedTV,
    this.favoriteMovies,
  });
}

final class MoviesError extends MoviesState {
  final String errorText;

  MoviesError({required this.errorText});
}
