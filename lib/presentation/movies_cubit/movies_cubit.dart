import 'package:cinema_firebase_edition/domain/movie_usecase.dart';
import 'package:cinema_firebase_edition/presentation/movies_cubit/movies_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MoviesCubit extends Cubit<MoviesState> {
  final MoviesUsecase usecase;
  MoviesCubit({required this.usecase}) : super(MoviesInitial());

  Future<void> getFavoriteMovies() async {
    emit(MoviesLoading());
    try {
      await usecase.getFavoriteMovies();
      emit(globalState());
    } catch (e, stackTrace) {
      emit(MoviesError(errorText: "${e.toString()}\n$stackTrace"));
    }
  }

  Future<void> getExploreMovies() async {
    emit(MoviesLoading());
    try {
      await usecase.getExploreMovies();

      emit(globalState());
    } catch (e, stackTrace) {
      emit(MoviesError(errorText: "${e.toString()}\n$stackTrace"));
    }
  }

  Future<void> getPopularMovies() async {
    emit(MoviesLoading());
    try {
      await usecase.getPopularMovies();
      emit(globalState());
    } catch (e, stackTrace) {
      emit(MoviesError(errorText: "${e.toString()}\n$stackTrace"));
    }
  }

  Future<void> getPopularMoviesInSwipe() async {
    emit(MoviesLoading());
    try {
      await usecase.getPopularMoviesInSwipe();
      emit(globalState());
    } catch (e, stackTrace) {
      emit(MoviesError(errorText: "${e.toString()}\n$stackTrace"));
    }
  }

  Future<void> getMoviesBySearch(String query) async {
    emit(MoviesLoading());
    try {
      await usecase.getMoviesBySearch(query);
      emit(globalState());
    } catch (e, stackTrace) {
      emit(MoviesError(errorText: "${e.toString()}\n$stackTrace"));
    }
  }

  Future<void> getMovieDetailsById(int id) async {
    emit(MoviesLoading());
    try {
      await usecase.getMovieDetailsById(id);
      emit(globalState());
    } catch (e, stackTrace) {
      emit(MoviesError(errorText: "${e.toString()}\n$stackTrace"));
    }
  }

  Future<void> getTVDetailsById(int id) async {
    emit(MoviesLoading());
    try {
      await usecase.getTvDetailsById(id);
      emit(globalState());
    } catch (e, stackTrace) {
      emit(MoviesError(errorText: "${e.toString()}\n$stackTrace"));
    }
  }

  MoviesSuccess globalState() => MoviesSuccess(
        movies: usecase.movies,
        moviesFor2024: usecase.moviesFor2024,
        movieDetails: usecase.movieDetails,
        topRatedTV: usecase.topRatedTV,
        favoriteMovies: usecase.favoriteMovies,
      );
}
