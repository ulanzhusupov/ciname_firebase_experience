import 'package:cinema_firebase_edition/data/repository/firebase_service.dart';
import 'package:cinema_firebase_edition/presentation/auth_cubit/auth_state.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AuthCubit extends Cubit<AuthState> {
  final FirebaseService firebaseService;

  AuthCubit({required this.firebaseService}) : super(AuthInitial());

  Future<void> createAccount({
    required String email,
    required String password,
  }) async {
    emit(AuthLoading());

    try {
      final result =
          await firebaseService.createAccount(email: email, password: password);
      emit(AuthSuccess(credential: result));
    } catch (e) {
      if (e is FirebaseAuthException) {
        emit(AuthError(errorText: e.message ?? ""));
      } else {
        emit(AuthError(errorText: e.toString()));
      }
    }
  }
}
