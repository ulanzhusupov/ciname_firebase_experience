import 'package:firebase_auth/firebase_auth.dart';

sealed class AuthState {}

class AuthInitial extends AuthState {}

class AuthLoading extends AuthState {}

class AuthSuccess extends AuthState {
  final UserCredential credential;

  AuthSuccess({required this.credential});
}

class AuthError extends AuthState {
  final String errorText;

  AuthError({required this.errorText});
}
