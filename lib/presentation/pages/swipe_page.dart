import 'package:cinema_firebase_edition/hive/hive_movie_model.dart';
import 'package:cinema_firebase_edition/presentation/movies_cubit/movies_cubit.dart';
import 'package:cinema_firebase_edition/presentation/movies_cubit/movies_state.dart';
import 'package:cinema_firebase_edition/presentation/theme/app_colors.dart';
import 'package:cinema_firebase_edition/presentation/theme/app_fonts.dart';
import 'package:cinema_firebase_edition/presentation/widgets/my_chip.dart';
import 'package:cinema_firebase_edition/presentation/widgets/tinder_movie_card.dart';
import 'package:cinema_firebase_edition/resources/resources.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:swipable_stack/swipable_stack.dart';

class SwipePage extends StatefulWidget {
  const SwipePage({super.key});

  @override
  State<SwipePage> createState() => _SwipePageState();
}

class _SwipePageState extends State<SwipePage> {
  List<String> entertainments = ["Все", "Фильмы", "Сериалы", "ТВ", "Шоу"];
  int chipCurrentIndex = 0;
  late final SwipableStackController _controller;
  bool willMoveRight = false;
  void _listenController() => setState(() {});
  var moviesBox;

  @override
  void initState() {
    super.initState();
    _controller = SwipableStackController()..addListener(_listenController);
    BlocProvider.of<MoviesCubit>(context).getPopularMoviesInSwipe();

    initHive();
  }

  void initHive() async {
    moviesBox = await Hive.openBox("moviesBox");
    if (moviesBox.get("likedMovies") == null) {
      List<HiveMovieModel> likedMovies = [];
      await moviesBox.put("likedMovies", likedMovies);
    }
  }

  @override
  void dispose() {
    super.dispose();
    _controller
      ..removeListener(_listenController)
      ..dispose();
  }

  void putMovie(HiveMovieModel movie) async {
    HiveMovieModel model = HiveMovieModel(
      id: movie.id,
      adult: movie.adult,
      backdropPath: movie.backdropPath,
      genreIds: movie.genreIds ?? [],
      originalLanguage: movie.originalLanguage,
      originalTitle: movie.originalTitle,
      overview: movie.overview,
      popularity: movie.popularity,
      posterPath: movie.posterPath,
      releaseDate: movie.releaseDate,
      title: movie.title,
      video: movie.video,
      voteAverage: movie.voteAverage,
      voteCount: movie.voteCount,
    );
    List fromHive = await moviesBox.get("likedMovies");
    int removedCount = 0;
    fromHive.removeWhere((element) {
      if (element.id == movie.id) {
        removedCount++;
        return true;
      }
      return false;
    });
    if (removedCount == 0) {
      fromHive.add(model);
    }
    await moviesBox.put("likedMovies", fromHive);
  }

  void printLeft() async {
    print("Swiped to left");
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: Container(
              width: double.infinity,
              height: 260.h,
              decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(MyIcons.bgExplore), fit: BoxFit.cover),
              ),
              child: Padding(
                padding: const EdgeInsets.only(
                  right: 16,
                  left: 16,
                  top: 44,
                  bottom: 24,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Быстрый обзор",
                      style: AppFonts.s28W700.copyWith(color: AppColors.white),
                    ),
                    SizedBox(height: 8.h),
                    SizedBox(
                      height: 60,
                      child: ListView.builder(
                        itemCount: entertainments.length,
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) => MyChip(
                          isSelected: chipCurrentIndex == index,
                          title: entertainments[index],
                          onSelected: (val) {
                            chipCurrentIndex = index;
                            setState(() {});
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            top: 200.h,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const Icon(Icons.close),
                SizedBox(
                  width: 297.w,
                  height: 430.h,
                  child: BlocBuilder<MoviesCubit, MoviesState>(
                      builder: (context, state) {
                    if (state is MoviesLoading) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    }

                    if (state is MoviesError) {
                      return Text(
                        state.errorText,
                        style: AppFonts.s17W400,
                      );
                    }

                    if (state is MoviesSuccess) {
                      return SwipableStack(
                        itemCount: state.movies?.length,
                        detectableSwipeDirections: const {
                          SwipeDirection.right,
                          SwipeDirection.left,
                        },
                        controller: _controller,
                        stackClipBehaviour: Clip.none,
                        onSwipeCompleted: (index, direction) {
                          willMoveRight = false;
                        },
                        onWillMoveNext: (index, swipeDirection) {
                          if (swipeDirection == SwipeDirection.right) {
                            willMoveRight = true;
                            putMovie(state.movies![index]);
                            return true;
                          } else if (swipeDirection == SwipeDirection.left) {
                            printLeft();
                            return true;
                          }
                          willMoveRight = false;
                          return false;
                        },
                        horizontalSwipeThreshold: 1,
                        verticalSwipeThreshold: 0.9,
                        allowVerticalSwipe: false,
                        builder: (context, properties) {
                          final itemIndex =
                              properties.index % (state.movies?.length ?? 0);

                          return TinderMovieCard(
                            image: state.movies![itemIndex].posterPath ?? "",
                            title: state.movies![itemIndex].title ?? "",
                            year: state.movies![itemIndex].releaseDate ?? "",
                            rating: "${state.movies![itemIndex].voteAverage}",
                          );
                        },
                      );
                    }

                    return const SizedBox();
                  }),
                ),
                Icon(
                  Icons.favorite,
                  size: willMoveRight ? 34 : 24,
                  color: willMoveRight ? Colors.red : AppColors.primary,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
