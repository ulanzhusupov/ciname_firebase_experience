import 'package:auto_route/auto_route.dart';
import 'package:cinema_firebase_edition/core/constants/app_consts.dart';
import 'package:cinema_firebase_edition/data/models/movie_details_model.dart';
import 'package:cinema_firebase_edition/hive/hive_movie_model.dart';
import 'package:cinema_firebase_edition/presentation/movies_cubit/movies_cubit.dart';
import 'package:cinema_firebase_edition/presentation/movies_cubit/movies_state.dart';
import 'package:cinema_firebase_edition/presentation/theme/app_colors.dart';
import 'package:cinema_firebase_edition/presentation/theme/app_fonts.dart';
import 'package:cinema_firebase_edition/presentation/widgets/poster_cache_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hive_flutter/hive_flutter.dart';

@RoutePage()
class MoviePage extends StatefulWidget {
  final int movieId;
  final bool? isTv;
  const MoviePage({
    super.key,
    required this.movieId,
    this.isTv,
  });

  @override
  State<MoviePage> createState() => _MoviePageState();
}

class _MoviePageState extends State<MoviePage> {
  // ignore: prefer_typing_uninitialized_variables
  var moviesBox;
  bool movieLiked = false;
  late List likedMovies;

  @override
  void initState() {
    super.initState();
    if (widget.isTv != null) {
      getTvDetails(widget.movieId);
    } else {
      getMovieDetails(widget.movieId);
    }
    initHive();
  }

  Future<void> initHive() async {
    moviesBox = await Hive.openBox("moviesBox");
    if (moviesBox.get("likedMovies") == null) {
      List<HiveMovieModel> likedMoviess = [];
      await moviesBox.put("likedMovies", likedMoviess);
    }
    likedMovies = await moviesBox.get("likedMovies");
    for (var element in likedMovies) {
      print(element.title);
      if (element.id == widget.movieId) {
        movieLiked = !movieLiked;
      }
    }
    setState(() {});
  }

  void getTvDetails(int id) {
    BlocProvider.of<MoviesCubit>(context).getTVDetailsById(id);
  }

  void getMovieDetails(int id) {
    BlocProvider.of<MoviesCubit>(context).getMovieDetailsById(id);
  }

  Future<void> addToFavorite(MovieDetailsModel? details) async {
    HiveMovieModel model = HiveMovieModel(
      id: details?.id,
      adult: details?.adult,
      backdropPath: details?.backdropPath,
      genreIds: [],
      originalLanguage: details?.originalLanguage,
      originalTitle: details?.originalTitle,
      overview: details?.overview,
      popularity: details?.popularity,
      posterPath: details?.posterPath,
      releaseDate: details?.releaseDate,
      title: details?.title,
      video: details?.video,
      voteAverage: details?.voteAverage,
      voteCount: details?.voteCount,
    );
    int removedCount = 0;
    likedMovies.removeWhere((element) {
      if (element.id == widget.movieId) {
        removedCount++;
        return true;
      }
      return false;
    });
    if (removedCount == 0) {
      likedMovies.add(model);
    }
    await moviesBox.put("likedMovies", likedMovies);
    setState(() {
      movieLiked = !movieLiked;
    });
  }

  String formatDate(String date) {
    List<String> dateList = date.split("-");
    List<String> months = [
      "Январь",
      "Февраль",
      "Март",
      "Апрель",
      "Май",
      "Июнь",
      "Июль",
      "Август",
      "Сентябрь",
      "Октябрь",
      "Ноябрь",
      "Декабрь",
    ];

    return "${dateList[2]} ${months[int.parse(dateList[1]) - 1]} ${dateList[0]} г.";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<MoviesCubit, MoviesState>(builder: (context, state) {
        if (state is MoviesLoading) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        if (state is MoviesError) {
          return Center(
            child: Text(
              state.errorText,
              style: AppFonts.s17W400,
            ),
          );
        }

        if (state is MoviesSuccess) {
          return CustomScrollView(
            slivers: [
              SliverAppBar(
                expandedHeight: 475.h,
                flexibleSpace: FlexibleSpaceBar(
                  background: PosterCacheImage(
                    imageUrl:
                        "${AppConsts.imageUrl}${state.movieDetails?.posterPath}",
                    imgH: 475.h,
                    boxFit: BoxFit.cover,
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  padding: const EdgeInsets.all(16.0),
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: MediaQuery.of(context).size.width * 0.7,
                                child: Text(
                                  state.movieDetails?.title ?? "",
                                  style: AppFonts.s22W700,
                                ),
                              ),
                              Text(
                                formatDate(state.movieDetails?.releaseDate ??
                                    "2024-01-01"),
                                style: AppFonts.s22W700
                                    .copyWith(color: AppColors.grey),
                              ),
                            ],
                          ),
                          IconButton(
                            onPressed: () {
                              addToFavorite(state.movieDetails);
                            },
                            icon: movieLiked
                                ? Icon(
                                    Icons.favorite,
                                    color: Colors.red,
                                    size: 36.w,
                                  )
                                : Icon(
                                    Icons.favorite_outline_outlined,
                                    size: 36.w,
                                  ),
                          ),
                        ],
                      ),
                      SizedBox(height: 39.h),
                      const Divider(),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 24.h),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Column(
                              children: [
                                Text(
                                  (state.movieDetails?.voteAverage ?? 0)
                                      .toStringAsFixed(1),
                                  style: AppFonts.s15W600,
                                ),
                                Text(
                                  "Кинопоиск",
                                  style: AppFonts.s13W400
                                      .copyWith(color: AppColors.grey),
                                ),
                              ],
                            ),
                            Column(
                              children: [
                                Text(
                                  state.movieDetails?.status ?? "",
                                  style: AppFonts.s15W600,
                                ),
                                Text(
                                  "Статус",
                                  style: AppFonts.s13W400
                                      .copyWith(color: AppColors.grey),
                                ),
                              ],
                            ),
                            Column(
                              children: [
                                Text(
                                  state.movieDetails?.originalLanguage ?? "",
                                  style: AppFonts.s15W600,
                                ),
                                Text(
                                  "Язык",
                                  style: AppFonts.s13W400
                                      .copyWith(color: AppColors.grey),
                                ),
                              ],
                            ),
                            Column(
                              children: [
                                Text(
                                  "\$${(state.movieDetails?.budget ?? 1) / 1000000} млн",
                                  style: AppFonts.s15W600,
                                ),
                                Text(
                                  "Бюджет",
                                  style: AppFonts.s13W400
                                      .copyWith(color: AppColors.grey),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      const Divider(),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 32.h),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            const Text(
                              "Описание",
                              style: AppFonts.s17W600,
                            ),
                            SizedBox(height: 8.h),
                            Text(
                              state.movieDetails?.overview ?? "",
                              style: AppFonts.s17W400
                                  .copyWith(color: AppColors.grey),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          );
        }

        return const SizedBox();
      }),
    );
  }
}
