import 'package:auto_route/auto_route.dart';
import 'package:cinema_firebase_edition/presentation/pages/auth/profile_page.dart';
import 'package:cinema_firebase_edition/presentation/pages/explore_page.dart';
import 'package:cinema_firebase_edition/presentation/pages/favorite_page.dart';
import 'package:cinema_firebase_edition/presentation/pages/search_page.dart';
import 'package:cinema_firebase_edition/presentation/pages/swipe_page.dart';
import 'package:cinema_firebase_edition/presentation/theme/app_colors.dart';
import 'package:cinema_firebase_edition/resources/resources.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

@RoutePage()
class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 2;
  static const List<Widget> _screens = <Widget>[
    ExplorePage(),
    SearchPage(),
    SwipePage(),
    FavoritePage(),
    ProfilePage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _screens[_selectedIndex],
      ),
      bottomNavigationBar: CurvedNavigationBar(
        backgroundColor: Colors.transparent,
        buttonBackgroundColor: AppColors.primary,
        items: [
          SvgPicture.asset(
            MyIcons.film,
            width: 24,
            color: _selectedIndex != 0 ? AppColors.grey : AppColors.white,
          ),
          SvgPicture.asset(
            MyIcons.search,
            color: _selectedIndex != 1 ? AppColors.grey : AppColors.white,
          ),
          SvgPicture.asset(
            MyIcons.swipe,
            width: 24,
            color: _selectedIndex != 2 ? AppColors.grey : AppColors.white,
          ),
          SvgPicture.asset(
            MyIcons.tickets,
            width: 24,
            color: _selectedIndex != 3 ? AppColors.grey : AppColors.white,
          ),
          SvgPicture.asset(
            MyIcons.accountCircle,
            width: 24,
            color: _selectedIndex != 4 ? AppColors.grey : AppColors.white,
          ),
        ],
        index: _selectedIndex,
        onTap: (index) {
          setState(() {
            _selectedIndex = index;
          });
        },
      ),
    );
  }
}
