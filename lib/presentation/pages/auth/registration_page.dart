import 'package:auto_route/auto_route.dart';
import 'package:cinema_firebase_edition/presentation/auth_cubit/auth_cubit.dart';
import 'package:cinema_firebase_edition/presentation/auth_cubit/auth_state.dart';
import 'package:cinema_firebase_edition/presentation/theme/app_colors.dart';
import 'package:cinema_firebase_edition/presentation/theme/app_fonts.dart';
import 'package:cinema_firebase_edition/resources/resources.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

@RoutePage()
class RegistrationPage extends StatelessWidget {
  const RegistrationPage({super.key});

  @override
  Widget build(BuildContext context) {
    final TextEditingController emailController = TextEditingController();
    final TextEditingController passwordController = TextEditingController();
    return Scaffold(
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: double.infinity,
                height: 132.h,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(MyIcons.bgTop),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Row(
                  children: [
                    SizedBox(width: 16.h),
                    Text(
                      "Профиль",
                      style: AppFonts.s28W700.copyWith(color: AppColors.white),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 48.h),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      "Привет,\nзарегистрируйте профиль.",
                      style: AppFonts.s28W700,
                    ),
                    SizedBox(height: 24.h),
                    Row(
                      children: [
                        Text(
                          "Если у вас уже есть профиль / ",
                          style:
                              AppFonts.s17W600.copyWith(color: AppColors.grey),
                        ),
                        const Flexible(
                          child: Text(
                            "Войдите",
                            style: AppFonts.s17W600,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 48.h),
                    TextField(
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        hintText: "Email",
                        hintStyle:
                            AppFonts.s17W400.copyWith(color: AppColors.grey),
                        filled: true,
                        fillColor: AppColors.white,
                        border: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                    ),
                    SizedBox(height: 20.h),
                    TextField(
                      decoration: InputDecoration(
                        hintText: "Пароль",
                        hintStyle:
                            AppFonts.s17W400.copyWith(color: AppColors.grey),
                        filled: true,
                        fillColor: AppColors.white,
                        border: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                    ),
                    SizedBox(height: 48.h),
                    SizedBox(
                      width: double.infinity,
                      height: 56.h,
                      child: ElevatedButton(
                        onPressed: () {
                          BlocProvider.of<AuthCubit>(context).createAccount(
                            email: emailController.text,
                            password: passwordController.text,
                          );
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: AppColors.primary,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(16.0),
                          ),
                        ),
                        child: Text(
                          "Регистрация",
                          style: AppFonts.s22W500.copyWith(
                            color: AppColors.white,
                          ),
                        ),
                      ),
                    ),
                    BlocBuilder<AuthCubit, AuthState>(
                      builder: (context, state) {
                        if (state is AuthError) {
                          return Center(
                              child: Text(
                            state.errorText,
                            style: AppFonts.s17W400,
                          ));
                        }
                        if (state is AuthLoading) {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        }
                        if (state is AuthSuccess) {
                          return const Center(
                              child: Text(
                            "OK",
                            style: AppFonts.s17W400,
                          ));
                        }
                        return const SizedBox();
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
