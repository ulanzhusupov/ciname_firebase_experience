import 'package:auto_route/auto_route.dart';
import 'package:cinema_firebase_edition/presentation/theme/app_colors.dart';
import 'package:cinema_firebase_edition/presentation/theme/app_fonts.dart';
import 'package:cinema_firebase_edition/resources/resources.dart';
import 'package:cinema_firebase_edition/routes/app_router.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

@RoutePage()
class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: double.infinity,
              height: 132.h,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(MyIcons.bgTop),
                  fit: BoxFit.cover,
                ),
              ),
              child: Row(
                children: [
                  SizedBox(width: 16.h),
                  Text(
                    "Профиль",
                    style: AppFonts.s28W700.copyWith(color: AppColors.white),
                  ),
                ],
              ),
            ),
            SizedBox(height: 48.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Привет,\nвойдите в профиль.",
                    style: AppFonts.s28W700,
                  ),
                  SizedBox(height: 24.h),
                  Row(
                    children: [
                      Text(
                        "Если его нет / ",
                        style: AppFonts.s17W600.copyWith(color: AppColors.grey),
                      ),
                      InkWell(
                        onTap: () {
                          context.router.push(const RegistrationRoute());
                        },
                        child: Text(
                          "Создайте новый",
                          style: AppFonts.s17W600,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 48.h),
                  TextField(
                    decoration: InputDecoration(
                      hintText: "Email",
                      hintStyle:
                          AppFonts.s17W400.copyWith(color: AppColors.grey),
                      filled: true,
                      fillColor: AppColors.white,
                      border: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                  SizedBox(height: 20.h),
                  TextField(
                    decoration: InputDecoration(
                      hintText: "Пароль",
                      hintStyle:
                          AppFonts.s17W400.copyWith(color: AppColors.grey),
                      filled: true,
                      fillColor: AppColors.white,
                      border: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                  SizedBox(height: 20.h),
                  Row(
                    children: [
                      Text(
                        "Забыли пароль? / ",
                        style: AppFonts.s17W600.copyWith(color: AppColors.grey),
                      ),
                      Text(
                        "Сбросить",
                        style: AppFonts.s17W600,
                      ),
                    ],
                  ),
                  SizedBox(height: 48.h),
                  SizedBox(
                    width: double.infinity,
                    height: 56.h,
                    child: ElevatedButton(
                      onPressed: () {},
                      style: ElevatedButton.styleFrom(
                        backgroundColor: AppColors.primary,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(16.0),
                        ),
                      ),
                      child: Text(
                        "Логин",
                        style: AppFonts.s22W500.copyWith(
                          color: AppColors.white,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
