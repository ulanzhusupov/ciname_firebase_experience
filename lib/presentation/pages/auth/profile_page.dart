import 'package:cinema_firebase_edition/presentation/theme/app_colors.dart';
import 'package:cinema_firebase_edition/presentation/theme/app_fonts.dart';
import 'package:cinema_firebase_edition/resources/resources.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 216.h,
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Positioned(
                    top: 0,
                    left: 0,
                    right: 0,
                    child: Container(
                      width: double.infinity,
                      height: 137.h,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(MyIcons.bgTop),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child: Row(
                        children: [
                          SizedBox(width: 16.h),
                          Text(
                            "username",
                            style: AppFonts.s28W700
                                .copyWith(color: AppColors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 0.h,
                    child: CircleAvatar(
                      backgroundColor: Colors.red,
                      radius: 60.w,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 32.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Email",
                    style: AppFonts.s17W600.copyWith(color: AppColors.grey),
                  ),
                  SizedBox(height: 8.h),
                  TextField(
                    enabled: false,
                    decoration: InputDecoration(
                      hintText: "drpepper@google.com",
                      hintStyle:
                          AppFonts.s17W400.copyWith(color: AppColors.black),
                      filled: true,
                      fillColor: AppColors.white,
                      border: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                  SizedBox(height: 53.h),
                  Text(
                    "Настройки",
                    style: AppFonts.s17W600.copyWith(color: AppColors.grey),
                  ),
                  SizedBox(height: 8.h),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 16.h),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Push уведомления",
                          style:
                              AppFonts.s17W600.copyWith(color: AppColors.black),
                        ),
                        Switch(value: false, onChanged: (val) {}),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 16.h),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Подписка на новости",
                          style:
                              AppFonts.s17W600.copyWith(color: AppColors.black),
                        ),
                        Switch(value: true, onChanged: (val) {}),
                      ],
                    ),
                  ),
                  SizedBox(height: 50.h),
                  const Divider(),
                  SizedBox(height: 17.h),
                  Center(
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 14.h),
                      child: InkWell(
                        onTap: () {},
                        child: Text(
                          "Выйти из профиля",
                          style:
                              AppFonts.s17W600.copyWith(color: AppColors.red),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
