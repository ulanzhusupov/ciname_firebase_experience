import 'package:cinema_firebase_edition/presentation/movies_cubit/movies_cubit.dart';
import 'package:cinema_firebase_edition/presentation/movies_cubit/movies_state.dart';
import 'package:cinema_firebase_edition/presentation/theme/app_colors.dart';
import 'package:cinema_firebase_edition/presentation/theme/app_fonts.dart';
import 'package:cinema_firebase_edition/presentation/widgets/big_carousel.dart';
import 'package:cinema_firebase_edition/presentation/widgets/small_carousel.dart';
import 'package:cinema_firebase_edition/resources/resources.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:infinite_carousel/infinite_carousel.dart';

class ExplorePage extends StatefulWidget {
  const ExplorePage({super.key});

  @override
  State<ExplorePage> createState() => _ExplorePageState();
}

class _ExplorePageState extends State<ExplorePage> {
  var moviesBox;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<MoviesCubit>(context).getExploreMovies();
  }

  @override
  Widget build(BuildContext context) {
    final InfiniteScrollController carouselController =
        InfiniteScrollController();

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              height: 495.h,
              child: Stack(
                alignment: Alignment.topCenter,
                children: [
                  Container(
                    width: double.infinity,
                    height: 260.h,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(MyIcons.bgExplore),
                          fit: BoxFit.cover),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Column(
                        children: [
                          SizedBox(height: 44.h),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Новое и актуальное",
                                style: AppFonts.s28W700
                                    .copyWith(color: AppColors.white),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    top: 120,
                    left: 0,
                    right: 0,
                    child: SizedBox(
                      height: 368.h,
                      child: BlocBuilder<MoviesCubit, MoviesState>(
                        builder: (context, state) {
                          if (state is MoviesLoading) {
                            return const Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                          if (state is MoviesError) {
                            return Center(
                              child: Text(
                                state.errorText,
                                style: AppFonts.s17W400,
                              ),
                            );
                          }
                          if (state is MoviesSuccess) {
                            return BigCarousel(
                              carouselController: carouselController,
                              movies2024: state.moviesFor2024 ?? [],
                            );
                          }

                          return const SizedBox();
                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 19.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.h),
              child: const Text(
                "Популярно сейчас",
                style: AppFonts.s28W700,
              ),
            ),
            SizedBox(height: 24.h),
            SizedBox(
              height: 187.h,
              child: BlocBuilder<MoviesCubit, MoviesState>(
                builder: (context, state) {
                  if (state is MoviesLoading) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  if (state is MoviesError) {
                    return Center(
                      child: Text(
                        state.errorText,
                        style: AppFonts.s17W400,
                      ),
                    );
                  }

                  if (state is MoviesSuccess) {
                    return SmallCarousel(
                      carouselController: carouselController,
                      movies: state.movies ?? [],
                    );
                  }
                  return const SizedBox();
                },
              ),
            ),
            SizedBox(height: 19.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.h),
              child: const Text(
                "Популярные сериалы",
                style: AppFonts.s28W700,
              ),
            ),
            SizedBox(height: 24.h),
            SizedBox(
              height: 187.h,
              child: BlocBuilder<MoviesCubit, MoviesState>(
                builder: (context, state) {
                  if (state is MoviesLoading) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  if (state is MoviesError) {
                    return Center(
                      child: Text(
                        state.errorText,
                        style: AppFonts.s17W400,
                      ),
                    );
                  }

                  if (state is MoviesSuccess) {
                    return SmallCarousel(
                      carouselController: carouselController,
                      movies: state.topRatedTV ?? [],
                      isTv: true,
                    );
                  }
                  return const SizedBox();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
