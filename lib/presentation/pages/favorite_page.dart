import 'package:auto_route/auto_route.dart';
import 'package:cinema_firebase_edition/core/constants/app_consts.dart';
import 'package:cinema_firebase_edition/presentation/movies_cubit/movies_cubit.dart';
import 'package:cinema_firebase_edition/presentation/movies_cubit/movies_state.dart';
import 'package:cinema_firebase_edition/presentation/theme/app_colors.dart';
import 'package:cinema_firebase_edition/presentation/theme/app_fonts.dart';
import 'package:cinema_firebase_edition/presentation/widgets/poster_cache_image.dart';
import 'package:cinema_firebase_edition/resources/resources.dart';
import 'package:cinema_firebase_edition/routes/app_router.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FavoritePage extends StatefulWidget {
  const FavoritePage({super.key});

  @override
  State<FavoritePage> createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<MoviesCubit>(context).getFavoriteMovies();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          top: 136.h,
          left: 0,
          right: 0,
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Column(
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.75,
                  child: BlocBuilder<MoviesCubit, MoviesState>(
                    builder: (context, state) {
                      if (state is MoviesLoading) {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      }

                      if (state is MoviesSuccess) {
                        return GridView.count(
                          primary: false,
                          padding: const EdgeInsets.symmetric(
                              vertical: 20, horizontal: 16),
                          childAspectRatio: 0.7,
                          crossAxisSpacing: 0,
                          mainAxisSpacing: 10,
                          crossAxisCount: 2,
                          children: state.favoriteMovies
                                  ?.map((e) => ClipRRect(
                                        borderRadius: BorderRadius.circular(15),
                                        child: InkWell(
                                          onTap: () {
                                            context.router.push(
                                                MovieRoute(movieId: e.id ?? 0));
                                          },
                                          child: PosterCacheImage(
                                            imageUrl:
                                                "${AppConsts.imageUrl}${e.posterPath}",
                                          ),
                                        ),
                                      ))
                                  .toList() ??
                              [],
                        );
                      }

                      if (state is MoviesError) {
                        return Center(
                          child: Text(
                            state.errorText,
                            style: AppFonts.s17W400,
                          ),
                        );
                      }

                      return Center(
                        child: Text(
                          "Список понравившихся пуст",
                          style:
                              AppFonts.s17W400.copyWith(color: AppColors.grey),
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          top: 0,
          left: 0,
          right: 0,
          child: Container(
            width: double.infinity,
            height: 137.h,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage(MyIcons.bgTop),
                fit: BoxFit.cover,
              ),
              color: Color.fromRGBO(0, 0, 0, 0),
            ),
            child: Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    "Понравилось",
                    style: AppFonts.s28W700.copyWith(color: AppColors.white),
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
