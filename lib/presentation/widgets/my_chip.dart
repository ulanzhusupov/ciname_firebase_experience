import 'package:cinema_firebase_edition/presentation/theme/app_colors.dart';
import 'package:cinema_firebase_edition/presentation/theme/app_fonts.dart';
import 'package:flutter/material.dart';

class MyChip extends StatelessWidget {
  const MyChip({
    super.key,
    required this.isSelected,
    required this.title,
    required this.onSelected,
  });

  final bool isSelected;
  final String title;
  final Function(bool) onSelected;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 3),
      child: RawChip(
        elevation: 0,
        onSelected: onSelected,
        showCheckmark: false,
        selectedColor: AppColors.white,
        backgroundColor: AppColors.primary,
        label: Text(
          title,
          style: AppFonts.s17W400.copyWith(
            color: isSelected ? AppColors.primary : AppColors.white,
          ),
        ),
        selected: isSelected,
        shape: const StadiumBorder(
          side: BorderSide(
            color: Colors.transparent,
          ),
        ),
      ),
    );
  }
}
