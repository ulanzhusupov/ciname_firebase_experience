import 'package:auto_route/auto_route.dart';
import 'package:cinema_firebase_edition/core/constants/app_consts.dart';
import 'package:cinema_firebase_edition/hive/hive_movie_model.dart';
import 'package:cinema_firebase_edition/presentation/widgets/poster_cache_image.dart';
import 'package:cinema_firebase_edition/routes/app_router.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:infinite_carousel/infinite_carousel.dart';

class SmallCarousel extends StatelessWidget {
  const SmallCarousel({
    super.key,
    required this.carouselController,
    required this.movies,
    this.isTv,
  });

  final InfiniteScrollController carouselController;
  final List<HiveMovieModel> movies;
  final bool? isTv;

  @override
  Widget build(BuildContext context) {
    return InfiniteCarousel.builder(
      key: UniqueKey(),
      controller: carouselController,
      velocityFactor: 0.5,
      center: true,
      itemCount: movies.length,
      itemExtent: 130.w,
      itemBuilder: (context, itemIndex, realIndex) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
        child: ClipRRect(
          borderRadius: const BorderRadius.all(Radius.circular(20)),
          child: InkWell(
            onTap: () {
              context.router.push(MovieRoute(
                movieId: movies[itemIndex].id ?? 0,
                isTv: isTv,
              ));
            },
            child: PosterCacheImage(
              imageUrl: "${AppConsts.imageUrl}${movies[itemIndex].posterPath}",
              imgW: 128.w,
            ),
          ),
        ),
      ),
    );
  }
}
