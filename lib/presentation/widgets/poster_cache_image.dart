import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class PosterCacheImage extends StatelessWidget {
  const PosterCacheImage({
    super.key,
    required this.imageUrl,
    this.imgW,
    this.imgH,
    this.boxFit,
  });
  final String imageUrl;
  final double? imgW;
  final double? imgH;
  final BoxFit? boxFit;

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: imageUrl,
      width: imgW,
      height: imgH,
      fit: boxFit,
      placeholder: (context, url) {
        return const Center(
          child: CircularProgressIndicator(),
        );
      },
      errorWidget: (context, url, error) => const Icon(
        Icons.error,
        size: 24,
      ),
    );
  }
}
