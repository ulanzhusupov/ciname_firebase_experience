import 'package:auto_route/auto_route.dart';
import 'package:cinema_firebase_edition/core/constants/app_consts.dart';
import 'package:cinema_firebase_edition/hive/hive_movie_model.dart';
import 'package:cinema_firebase_edition/presentation/widgets/poster_cache_image.dart';
import 'package:cinema_firebase_edition/routes/app_router.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:infinite_carousel/infinite_carousel.dart';

class BigCarousel extends StatelessWidget {
  const BigCarousel({
    super.key,
    required this.carouselController,
    required this.movies2024,
  });

  final InfiniteScrollController carouselController;
  final List<HiveMovieModel> movies2024;

  @override
  Widget build(BuildContext context) {
    return InfiniteCarousel.builder(
      key: UniqueKey(),
      controller: carouselController,
      velocityFactor: 0.5,
      center: true,
      itemCount: movies2024.length,
      itemExtent: 264.w,
      itemBuilder: (context, itemIndex, realIndex) => ClipRRect(
        borderRadius: const BorderRadius.all(Radius.circular(20)),
        child: InkWell(
          onTap: () {
            context.router
                .push(MovieRoute(movieId: movies2024[itemIndex].id ?? 0));
          },
          child: PosterCacheImage(
              imageUrl:
                  "${AppConsts.imageUrl}${movies2024[itemIndex].posterPath}"),
        ),
      ),
    );
  }
}
