import 'dart:ui';

import 'package:cinema_firebase_edition/core/constants/app_consts.dart';
import 'package:cinema_firebase_edition/presentation/theme/app_colors.dart';
import 'package:cinema_firebase_edition/presentation/theme/app_fonts.dart';
import 'package:cinema_firebase_edition/presentation/widgets/poster_cache_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TinderMovieCard extends StatelessWidget {
  const TinderMovieCard({
    super.key,
    required this.image,
    required this.title,
    required this.year,
    required this.rating,
  });

  final String image;
  final String title;
  final String year;
  final String rating;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 297.w,
      height: 416.h,
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Positioned(
            top: 0,
            bottom: 0,
            // left: 0,
            // right: 0,
            child: ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(20)),
              child: PosterCacheImage(
                imageUrl: "${AppConsts.imageUrl}$image",
                imgW: 297.w,
                imgH: 416.h,
                boxFit: BoxFit.cover,
              ),
            ),
          ),
          ClipRRect(
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20),
            ),
            child: SizedBox(
              height: 95.h,
              child: BackdropFilter(
                filter: ImageFilter.blur(
                  sigmaX: 10,
                  sigmaY: 10,
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 14,
                    vertical: 10,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        title,
                        style: AppFonts.s18W600.copyWith(
                          color: AppColors.white,
                          height: 1,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            year,
                            style: AppFonts.s17W600.copyWith(
                              color: Colors.white.withOpacity(0.7),
                            ),
                          ),
                          Container(
                            decoration: const BoxDecoration(
                              color: Color.fromRGBO(255, 255, 255, 0.32),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 6,
                                vertical: 2,
                              ),
                              child: Row(
                                children: [
                                  const Icon(
                                    Icons.star,
                                    color: AppColors.primary,
                                    // size: 13.3,
                                  ),
                                  Text(
                                    rating,
                                    style: AppFonts.s15W600
                                        .copyWith(color: AppColors.white),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
