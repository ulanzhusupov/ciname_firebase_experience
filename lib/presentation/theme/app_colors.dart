import 'package:flutter/material.dart';

abstract class AppColors {
  static const LinearGradient appBarOrange = LinearGradient(
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
      colors: [
        Color(0xffFF986F),
        Color(0xffFB743D),
      ]);
  static const Color primary = Color(0xffFB743D);
  static const Color red = Color(0xffD62347);
  static const Color white = Color(0xffFFFFFF);
  static const Color black = Color(0xff1E263C);
  static Color black60 = const Color(0xff1E263C).withOpacity(0.6);
  static const Color grey = Color(0xff7A8B9E);
  static Color grey50 = const Color(0xff7A8B9E).withOpacity(0.5);
  static Color grey12 = const Color(0xff7A8B9E).withOpacity(0.12);
}
