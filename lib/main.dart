import 'package:cinema_firebase_edition/core/network/dio_settings.dart';
import 'package:cinema_firebase_edition/data/repository/firebase_service.dart';
import 'package:cinema_firebase_edition/data/repository/movie_repository.dart';
import 'package:cinema_firebase_edition/domain/movie_usecase.dart';
import 'package:cinema_firebase_edition/firebase_options.dart';
import 'package:cinema_firebase_edition/hive/hive_movie_model.dart';
import 'package:cinema_firebase_edition/presentation/auth_cubit/auth_cubit.dart';
import 'package:cinema_firebase_edition/presentation/movies_cubit/movies_cubit.dart';
import 'package:cinema_firebase_edition/routes/app_router.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/adapters.dart';

void main() async {
  await Hive.initFlutter();
  Hive.registerAdapter(HiveMovieModelAdapter());

  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final appRouter = AppRouter();
    return ScreenUtilInit(
      designSize: const Size(375, 812),
      minTextAdapt: true,
      splitScreenMode: true,
      child: MultiRepositoryProvider(
        providers: [
          RepositoryProvider(create: (context) => DioSettings()),
          RepositoryProvider(create: (context) => FirebaseService()),
          RepositoryProvider(
              create: (context) => MovieRepository(
                  dioSettings: RepositoryProvider.of<DioSettings>(context))),
          RepositoryProvider(
              create: (context) => MoviesUsecase(
                  repository: RepositoryProvider.of<MovieRepository>(context))),
        ],
        child: MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (context) => MoviesCubit(
                usecase: RepositoryProvider.of<MoviesUsecase>(context),
              ),
            ),
            BlocProvider(
              create: (context) => AuthCubit(
                firebaseService:
                    RepositoryProvider.of<FirebaseService>(context),
              ),
            ),
          ],
          child: MaterialApp.router(
            routerConfig: appRouter.config(),
            title: 'Flutter Demo',
            theme: ThemeData(
              colorScheme: ColorScheme.fromSeed(
                  seedColor: Colors.deepPurple,
                  background: const Color(0xfff6f6f6)),
              useMaterial3: true,
              fontFamily: GoogleFonts.prompt().fontFamily,
            ),
            debugShowCheckedModeBanner: false,
          ),
        ),
      ),
    );
  }
}
