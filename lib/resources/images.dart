part of 'resources.dart';

class Images {
  Images._();

  static const String cinemaPoster = 'assets/images/cinema_poster.png';
  static const String dune = 'assets/images/dune.png';
  static const String noImage = 'assets/images/no_image.png';
  static const String squad = 'assets/images/squad.png';
  static const String theTomorrowWar = 'assets/images/the_tomorrow_war.png';
  static const String wrathOfMan = 'assets/images/wrath_of_man.png';
}
