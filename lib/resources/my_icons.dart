part of 'resources.dart';

class MyIcons {
  MyIcons._();

  static const String accountCircle = 'assets/png/account_circle.svg';
  static const String bgExplore = 'assets/png/bg_explore.png';
  static const String bgTop = 'assets/png/bg_top.png';
  static const String film = 'assets/png/film.svg';
  static const String filterSolid = 'assets/png/filter_solid.png';
  static const String search = 'assets/png/search.svg';
  static const String searchBlack = 'assets/png/search_black.png';
  static const String swipe = 'assets/png/swipe.svg';
  static const String tickets = 'assets/png/tickets.svg';
}
